#USAP ecom solution docker image
FROM 681563092260.dkr.ecr.us-west-2.amazonaws.com/docker-web-base:php5.6

#declaration of variables
ENV DOCKER_SETUP_DIR /tmp/docker-setup

# Install any needed packages specified in requirements.txt
COPY requirements.txt $DOCKER_SETUP_DIR/

RUN yum -y install `cat $DOCKER_SETUP_DIR/requirements.txt`; yum clean all

#copy the needed bash script
COPY ./scripts $DOCKER_SETUP_DIR/scripts

#copy all other needed files from host to docker instance
COPY ./conf $DOCKER_SETUP_DIR/conf
COPY ./configs $DOCKER_SETUP_DIR/configs

#Install other packages
RUN chmod +x $DOCKER_SETUP_DIR/scripts/start.sh

#open port
EXPOSE 80 443

ENTRYPOINT $DOCKER_SETUP_DIR/scripts/start.sh
