#!/usr/bin/env bash

#Instal Zend Framework and Debug
cp $DOCKER_SETUP_DIR/packages/ZendFramework-1.12.7.tar.gz /usr/share/php/
cp $DOCKER_SETUP_DIR/packages/ZFDebug.tar /usr/share/php/

cd /usr/share/php/
tar -zxf ZendFramework-1.12.7.tar.gz
ln -s ZendFramework-1.12.7/library/Zend/
tar -zxf ZFDebug.tar

cp $DOCKER_SETUP_DIR/packages/Memcached.php /usr/share/php/ZendFramework-1.12.7/library/Zend/Cache/Backend

#copy http and ssl conf
cp $DOCKER_SETUP_DIR/conf/httpd.conf /etc/httpd/conf/
cp $DOCKER_SETUP_DIR/conf/ssl.conf /etc/httpd/conf.d/

#copy the default php.ini configuration
cp $DOCKER_SETUP_DIR/conf/php.ini /etc/php.ini

#export myaccount-module source code

mkdir -p /data/www/html/sites/
cd /data/www/html/sites/
git clone https://email2trac:USAP1q2w@bitbucket.org/usautoparts/myaccount-module.git platform-mycart-dcl

#confd

cp -r $DOCKER_SETUP_DIR/configs/toml/* /etc/confd/conf.d/
cp -r $DOCKER_SETUP_DIR/configs/tmpl/* /etc/confd/templates/
