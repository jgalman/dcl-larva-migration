#need to define $HOST_USER_NAME in docker run command
export CORE_DOMAIN="platform.discountcarlights.staging.usautoparts.com"
export CART_DOMAIN="platform.cart.discountcarlights.staging.usautoparts.com"
export ACCOUNT_DOMAIN="myaccount-staging-discountcarlights.usautoparts.com"
export CHECKOUT_DOMAIN="platform.checkout.discountcarlights.staging.usautoparts.com"
export STATIC_DOMAIN="static.discountcarlights.com"
export ACCOUNT_DIM_DOMAIN="myaccount.discountcarlights.staging.usautoparts.com"
export ENV="staging"
