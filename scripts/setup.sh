#create vhosts folder and vhost for ecom site

mkdir -p "/etc/httpd/conf.d/vhosts";
chmod -R 775 "/etc/httpd/conf.d/vhosts";
mkdir -p "/data/hydraLog";
chmod -R 777 "/data/hydraLog";
mkdir -p "/etc/httpd/conf.d/vhosts/vhosts";
cp $DOCKER_SETUP_DIR/conf/server.* /etc/httpd/conf.d/vhosts/vhosts;

#load the build configuration
. $DOCKER_SETUP_DIR/scripts/config.sh;

#update vhost file 
echo "updating vhost file....";
sed -i "s/%%MYCART_DIR%%/$(echo /data/www/html/sites/platform-mycart-dcl | sed 's/\//\\\//g')/g" $DOCKER_SETUP_DIR/conf/ecom.vhost
sed -i "s/%%STATIC_DIR%%/$(echo /data/www/html/ | sed 's/\//\\\//g')/g" $DOCKER_SETUP_DIR/conf/ecom.vhost
sed -i "s/%%ACCOUNT_DOMAIN%%/$(echo $ACCOUNT_DOMAIN | sed 's/\//\\\//g')/g" $DOCKER_SETUP_DIR/conf/ecom.vhost
sed -i "s/%%STATIC_DOMAIN%%/$(echo $STATIC_DOMAIN | sed 's/\//\\\//g')/g" $DOCKER_SETUP_DIR/conf/ecom.vhost
sed -i "s/%%HTTP_LOG_DIR%%/$(echo /var/log/httpd | sed 's/\//\\\//g')/g" $DOCKER_SETUP_DIR/conf/ecom.vhost
sed -i "s/%%VHOSTS_DIR%%/$(echo $VHOSTS_DIR | sed 's/\//\\\//g')/g" $DOCKER_SETUP_DIR/conf/ecom.vhost
sed -i "s/%%ACCOUNT_DIM_DOMAIN%%/$(echo $ACCOUNT_DIM_DOMAIN | sed 's/\//\\\//g')/g" $DOCKER_SETUP_DIR/conf/ecom.vhost
sed -i "s/%%ENVIRONMENT%%/$(echo $ENV | sed 's/\//\\\//g')/g" $DOCKER_SETUP_DIR/conf/ecom.vhost

#installing vhost file
cp $DOCKER_SETUP_DIR/conf/ecom.vhost /etc/httpd/conf.d/vhosts/myaccount-module.vhost

#create a symlink of the production dir to development and create a cart, account and checkout symlink site
cd /data/www/html/sites/platform-mycart-dcl/application/views/skins/migration/;
ln -s myaccount.discountcarlights.com $ACCOUNT_DOMAIN;

case $ENV in
	"staging" )
	
	echo "updating r3 development config....";
	sed -i "s/%%ACCOUNT_DOMAIN%%/$ACCOUNT_DOMAIN/g" $DOCKER_SETUP_DIR/conf/fulloverride.developmentplus.ini.pattern;
	sed -i "s/%%STATIC_DOMAIN%%/$STATIC_DOMAIN/g" $DOCKER_SETUP_DIR/conf/fulloverride.developmentplus.ini.pattern;
	sed -i "s/%%SITE_ABBR%%/dcl/g" $DOCKER_SETUP_DIR/conf/fulloverride.developmentplus.ini.pattern;
	sed -i "s/%%MONGO_HOST%%/localhost/g" $DOCKER_SETUP_DIR/conf/fulloverride.developmentplus.ini.pattern;
	echo -e "\n" >> $DOCKER_SETUP_DIR/conf/fulloverride.developmentplus.ini.pattern;

	sed -i "s/%%ACCOUNT_DOMAIN%%/$ACCOUNT_DOMAIN/g" $DOCKER_SETUP_DIR/conf/mycart.fulloverride.developmentplus.ini.pattern;
	sed -i "s/%%STATIC_DOMAIN%%/$STATIC_DOMAIN/g" $DOCKER_SETUP_DIR/conf/mycart.fulloverride.developmentplus.ini.pattern;
	sed -i "s/%%ACCOUNT_DIM_DOMAIN%%/myaccount.discountcarlights.com/g" $DOCKER_SETUP_DIR/conf/mycart.fulloverride.developmentplus.ini.pattern;
	sed -i "s/%%SITE_ABBR%%/dcl/g" $DOCKER_SETUP_DIR/conf/mycart.fulloverride.developmentplus.ini.pattern;
	sed -i "s/%%MONGO_HOST%%/localhost/g" $DOCKER_SETUP_DIR/conf/mycart.fulloverride.developmentplus.ini.pattern;
	echo -e "\n" >> $DOCKER_SETUP_DIR/conf/mycart.fulloverride.developmentplus.ini.pattern;


	cd /data/www/html/sites/platform-mycart-dcl/application/configs/;
	mv staging staging-back;
	ln -s production staging;
	cd staging;
	ln -s myaccount.discountcarlights.com $ACCOUNT_DOMAIN;;
	
	"production" )

	cd /data/www/html/sites/platform-mycart-dcl/application/configs/;
	cd production;
	ln -s myaccount.discountcarlights.com $ACCOUNT_DOMAIN;;

	*)
esac

cd /data/www/html/sites/platform-mycart-dcl/application/controllers/siteControllers/$ENV;
ln -s ../../../views/skins/migration/myaccount.discountcarlights.com/all/sitespecific/siteControllers $ACCOUNT_DOMAIN;
